#include <dirent.h>
#include <err.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "count_functions_lines.h"

size_t max_lines = 25;

enum flags
{
    DFLAG = 1,
    AFLAG = 2,
    CPPFLAG = 4
};

static void print_name(const char *file_dir, const char *color, int indent)
{
    print_color(file_dir, color, indent);
    printf("\n");
}

static int is_c_file(char *name)
{
    size_t size = 0;
    if (!name)
        return 0;
    size = strlen(name);
    if (size > 2 && name[size - 1] == 'c' && name[size - 2] == '.')
        return 1;
    else if (size > 4)
    {
        // .cc
        if (name[size - 1] == 'c' && name[size - 2] == 'c'
            && name[size - 3] == '.')
            return 1;
        // .hh
        else if (name[size - 1] == 'h')
            return name[size - 2] == 'h' && name[size - 3] == '.';
        // .hxx
        else if (name[size - 1] == 'x')
            return name[size - 2] == 'x' && name[size - 3] == 'h'
                && name[size - 4] == '.';
        return 0;
    }
    return 0;
}

static int is_valid_dir(const char *name)
{
    if (!strcmp(name, ".") || !strcmp(name, "..") || !strcmp(name, ".git"))
        return 0;
    if (name[0] == '.')
        return 0;
    if (strstr(name, ".dSYM"))
        return 0;
    return 1;
}

static int browse_directory(const char *name, int indent, int all)
{
    DIR *dir = NULL;
    struct dirent *entry;

    if (!(dir = opendir(name)))
        return 0;

    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_DIR)
        {
            char path[1024];
            if (!is_valid_dir(entry->d_name))
                continue;
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            print_name(entry->d_name, "yellow", indent);
            browse_directory(path, indent + 2, all);
        }
        else if (is_c_file(entry->d_name))
        {
            char path[1024];
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            print_name(entry->d_name, "magenta", indent);
            if (all && !count(path, indent + 2))
                print_color("Error when opening file\n", "red", indent + 2);
            else if (!all && !simple_count(path, indent + 2))
                print_color("Error when opening file\n", "red", indent + 2);
        }
    }
    if (closedir(dir) == -1)
        exit(1);

    return 1;
}

int get_count_option(int argc, char *argv[])
{
    int opt = 0;
    int flag = 0;

    while ((opt = getopt(argc, argv, "dAp")) != -1)
    {
        switch (opt)
        {
        case 'd':
            flag |= DFLAG;
            break;
        case 'A':
            flag |= AFLAG;
            break;
        case 'p':
            flag |= CPPFLAG;
            break;
        default:
            break;
        }
    }

    return flag;
}

int main(int argc, char *argv[])
{
    int flag = get_count_option(argc, argv);

    if (argc < 4 && flag && argv[argc - 1][0] == '-')
        argv[argc] = ".";

    if (flag & CPPFLAG)
        max_lines = 50;

    if (flag & DFLAG && flag & AFLAG)
        browse_directory(argv[optind], 0, 1);
    else if (flag & DFLAG)
        browse_directory(argv[optind], 0, 0);
    else if (flag & AFLAG)
    {
        if (!count(argv[optind], 0))
            print_color("Error when opening file\n", "red", 2);
    }
    else
    {
        if (!simple_count(argv[optind], 0))
            print_color("Error when opening file\n", "red", 2);
    }

    return 0;
}
