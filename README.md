# Count function lines

    DESCRIPTION:
        This allow you to know how many lines you have in a function according to the EPITA coding style.

    USAGE:
        prototype:
            count_functions_lines [OPTIONS] [PATH]:

        options:
            -d - Allow you to use it directly in a directory
            -A - Allow you to see on each function the number of lines
            -p - Count for EPITA Cpp coding style

        path:
            Path of the file or the directory in which you want to count lines.

BOB & CO
