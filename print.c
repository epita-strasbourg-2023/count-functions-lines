#include <stdio.h>
#include <string.h>

#include "count_functions_lines.h"

extern size_t max_lines;

void print_color(const char *string, const char *color, int indent)
{
    if (!strcmp(color, "red"))
        printf("\033[0;31m");
    else if (!strcmp(color, "green"))
        printf("\033[0;32m");
    else if (!strcmp(color, "yellow"))
        printf("\033[0;33m");
    else if (!strcmp(color, "blue"))
        printf("\033[0;34m");
    else if (!strcmp(color, "magenta"))
        printf("\033[0;35m");
    else if (!strcmp(color, "cyan"))
        printf("\033[0;36m");
    printf("%*s%s\033[0m", indent, "", string);
}

void error_or_correct_lines(const char *message, const char *color, int indent)
{
    if (!strcmp(color, "red"))
        printf("\033[0;31m");
    else if (!strcmp(color, "green"))
        printf("\033[0;32m");
    else if (!strcmp(color, "yellow"))
        printf("\033[0;33m");
    else if (!strcmp(color, "blue"))
        printf("\033[0;34m");
    else if (!strcmp(color, "magenta"))
        printf("\033[0;35m");
    else if (!strcmp(color, "cyan"))
        printf("\033[0;36m");
    printf("%*s%s%zu lines.\n", indent, "", message, max_lines);
}
