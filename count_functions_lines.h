#ifndef COUNT_FUNCTION_LINES
#define COUNT_FUNCTION_LINES

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

void print_color(const char *string, const char *color, int indent);
void error_or_correct_lines(const char *message, const char *color, int indent);

int count(const char *filename, int indent);
int simple_count(const char *filename, int indent);

#endif
